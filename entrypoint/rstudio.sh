#!/usr/bin/env bash

set -ef -o pipefail

# Set defaults for environmental variables in case they are undefined
#
# USER must be rstudio, see https://github.com/rstudio/rstudio/issues/1663#issuecomment-401476714
USER=rstudio
USERID=${USERID:=1000}

## Configure user with a different USERID if requested.
if [ "$USERID" -ne 1000 ]
then
    echo Changing USER ID to ${USERID}
    usermod \
	-u ${USERID} \
	${USER}
    groupmod \
	-g ${USERID} \
	rstudio
    chown -R $USER /home/$USER
    usermod -a -G staff $USER
fi

# Need to source Mamba environment.
# Otherwise, R is not available.
source _activate_current_env.sh

exec "$@"
